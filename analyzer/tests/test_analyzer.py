from unittest import TestCase
from unittest.mock import patch

from analyzer import Analyzer

HTML = """
<html>
    <head>
        <meta></meta>
        <script></script>
    </head>
    <body>
        <div>
            <p>
                <div>
                    <div></div>
                </div>
            </p>
        </div>
        <div>
            <p></p>
        </div>
    </body>
</html>
"""


class TestAnalyzer(TestCase):
    def setUp(self):
        def conn_get(*args, **kwargs):
            # Faking connection to the URL
            class Response:
                text = HTML

            return Response()

        patch("analyzer.connection.BaseConnection.get", new=conn_get).start()
        self.analyzer = Analyzer()
        self.analyzer.load_url(url="https://www.google.com")

    def test_getting_common_tag(self):
        """ Test to see if common tag found as expected """
        common_tag = self.analyzer.most_common_tag()
        assert common_tag == "div"

    def test_getting_unique_tags(self):
        """ Test to see if getting unique tags works as expected """
        unique_tags = self.analyzer.unique_tags()
        assert unique_tags == {"html", "head", "script", "meta", "div", "p", "body"}

    def test_getting_longest_path(self):
        """ Test to see if getting most common tags works as expected """
        longest_path = self.analyzer.longest_path()
        assert longest_path == ["html", "body", "div", "p", "div", "div"]

    def test_private_method_get_paths(self):
        """ Test to see if private method _get_paths works as expected """
        expected = [
            [
                ["html", "head", "meta"],
                ["html", "head", "script"],
                ["html", "body", "div", "p", "div", "div"],
                ["html", "body", "div", "p"],
            ]
        ]
        paths = self.analyzer._get_paths()
        self.assertListEqual(paths, expected)

    def test_get_longest_path_with_common_tag(self):
        """ Test to see if getting longest path with common tag works as expected """
        longest_path_common = self.analyzer.longest_path_common()
        assert longest_path_common == ["html", "body", "div", "p", "div", "div"]
