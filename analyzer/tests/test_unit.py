from unittest import TestCase

from analyzer.utils import validate_url
from analyzer.exceptions import InvalidURL


class TestAnalyzerUtils(TestCase):
    def test_URL_passes_validation(self):
        """ Test if valid URL passes """
        # Should not raise exception
        validate_url("https://www.google.com")
        validate_url("http://www.google.com")

    def test_invalid_URL_not_passes_validation(self):
        """ Test if invalid URL not passes validaiton """
        with self.assertRaises(InvalidURL) as e:
            validate_url("I'm_not_valid_url")
