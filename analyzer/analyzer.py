from collections import Counter

from bs4 import BeautifulSoup

from .utils import validate_url
from .connection import BaseConnection
from .exceptions import NoTagsFound


class Analyzer:
    """ The Analyzer class providing insights about HTML documents. """

    def __init__(self) -> None:
        self.conn = BaseConnection()
        self.url = None
        self.html = None
        self.soup = None
        self.paths = None

    def load_url(self, url: str) -> None:
        """ Sends request to the target and loads the HTML """
        validate_url(url)  # Will raise exception. Catched in run.py
        self.url = url
        response = self.conn.get(url)
        self.html = response.text
        self.soup = BeautifulSoup(self.html, "html.parser")

    def unique_tags(self) -> set:
        """ Method that will get unique tags from HTML """
        tags = self.soup.find_all()
        unique_tags = set()
        for tag in tags:
            unique_tags.add(tag.name)
        return unique_tags

    def most_common_tag(self) -> str:
        """ Method that will get most common tag from HTML. """
        tags = self.soup.find_all()
        tags_list = [tag.name for tag in tags]
        counter = Counter(tags_list)
        most_common = counter.most_common(1)
        if len(most_common) > 0:
            return most_common[0][0]
        raise NoTagsFound(message="No tags found in the HTML.")

    def longest_path(self) -> list:
        """ Method that will return the longest path in the HTML """
        paths = self._get_paths()
        result = {"len": 0, "path": []}
        for path in paths:
            for subpath in path:
                if len(subpath) > result["len"]:
                    result["len"] = len(subpath)
                    result["path"] = subpath
        return result["path"]

    def _get_paths(self) -> list:
        """ Method that will return all paths """
        tags = self.soup.find_all(recursive=False)
        paths = []
        for tag in tags:
            path = []
            full_path = self._recursive_path(tag, path)
            paths.append(full_path)
        return paths

    def _recursive_path(self, tag, current_path) -> list:
        """ Method that uses recursion to calculate paths """
        tag_children = tag.find_all(recursive=False)
        if not tag_children:
            return [current_path + [tag.name]]
        result = []
        for tag_child in tag_children:
            new_current_path = current_path + [tag.name]
            result.extend(self._recursive_path(tag_child, new_current_path))
        return result

    def longest_path_common(self) -> list:
        """ Method that will return the longest path were most common tag used. """
        paths = self._get_paths()
        most_common = self.most_common_tag()
        result = {"len": 0, "path": []}
        for path in paths:
            for subpath in path:
                if subpath.count(most_common) > result["len"]:
                    result["len"] = subpath.count(most_common)
                    result["path"] = subpath
        return result["path"]