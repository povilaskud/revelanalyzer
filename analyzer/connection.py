import requests

from requests.adapters import HTTPAdapter
from urllib3.util import Retry

from .exceptions import RequestCritical

retry_strategy = Retry(
    total=3,
    status_forcelist=[429, 500, 502, 503, 504],
    allowed_methods=["GET"],
)
adapter = HTTPAdapter(max_retries=retry_strategy)

CRITICAL_CODES = [400, 401, 402, 403, 405, 404]


class BaseConnection:
    """
    The class that handles requests.
    A retry strategy is implemented as well. By default will retry request max 3 times.
    """

    def __init__(self) -> None:
        self.session = requests.Session()
        self.session.mount("https://", adapter)
        self.session.mount("http://", adapter)

        # Here we can implement common techniques for bot prevention. Ex: proxy support, agent rotation.

    def get(self, url: str) -> dict:
        """ Get method implementation """
        response = self.session.get(url)
        self._validate_response(response)
        return response

    def _validate_response(self, response: requests.models.Response) -> None:
        """ Method that validates the response """
        if response.status_code in CRITICAL_CODES:
            raise RequestCritical(
                message=f"Response from given url return a critical status code: {response.status_code}"
            )
