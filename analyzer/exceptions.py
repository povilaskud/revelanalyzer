class BaseException(Exception):
    """ Base Exception class """

    def __init__(self, message: str, *args: object):
        self.message = message

    def __str__(self) -> str:
        return f"{self.message}"


class InvalidURL(BaseException):
    """ URL validation exception """

    def __init__(self, message: str):
        super().__init__(message=message)


class RequestCritical(BaseException):
    """
    Exception fired when response http code in critical list
    Critical list: [400, 401, 402, 403, 405, 404]
    """

    def __init__(self, message: str):
        super().__init__(message=message)


class NoTagsFound(BaseException):
    """
    Exception fired if HTML has no tags at all.
    """

    def __init__(self, message: str):
        super().__init__(message=message)