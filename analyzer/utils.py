import re

from .exceptions import InvalidURL


def validate_url(url: str) -> bool:
    """
    Check that the url is valid.
    Must include http / https.
    """
    if "http" not in url:
        raise InvalidURL(message="URL must include http/https.")

    regex = re.compile(
        r"^(?:http)s?://"  # http:// or https://
        r"(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|"  # domain...
        r"localhost|"  # localhost...
        r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"  # ...or ip
        r"(?::\d+)?"  # optional port
        r"(?:/?|[/?]\S+)$",
        re.IGNORECASE,
    )

    if re.match(regex, url) is None:
        raise InvalidURL(message="URL is not valid. Please check it and try again.")

    return True  # The URL is valid.
