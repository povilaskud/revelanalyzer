import argparse

from analyzer import Analyzer
from analyzer.exceptions import BaseException, NoTagsFound


def main(url: str) -> None:
    analyzer = Analyzer()
    analyzer.load_url(url)

    unique_tags = analyzer.unique_tags()
    most_common_tags = analyzer.most_common_tag()
    longest_path = analyzer.longest_path()
    longest_path_common = analyzer.longest_path_common()

    print("----------------------------------------")
    print(f"Unique tags: {', '.join(unique_tags)}")
    print("---")
    print(f"Most common Tag: {most_common_tags}")
    print("---")
    print(f"Longest path: {' > '.join(longest_path)}")
    print("---")
    print(f"Longest path (common tag): {' > '.join(longest_path_common)}")
    print("----------------------------------------")

    return {
        "unique_tags": unique_tags,
        "most_common_tags": most_common_tags,
        "longest_path": longest_path,
        "longest_path_common": longest_path_common,
    }


if __name__ == "__main__":
    # NOTE: For the simplicity now using argparse. For more complex solution would go with Click.
    parser = argparse.ArgumentParser(
        description="Get the insights about HTML document."
    )
    parser.add_argument("--url", help="Specify an URL to get the insights.")
    arg = parser.parse_args()
    if arg.url is None:
        print("Please specify URL by providing --url parameter.")
        exit()
    try:
        main(url=arg.url)
    except BaseException as e:
        print(e.message)
