# Overview

This is a CLI application that can take a URL as an argument and grab insights about HTML.

_Created by:_ **Povilas Kudriavcevas**

# Requirements

- Python =>3.8
- Tested on Linux, but should work on macOS and Windows

# Setup

1.  Create and activate a virtual environment (python version => 3.8)

        virtualenv env -p python
        source env/bin/activate

2.  Install pip libraries

        pip install -r requirements.pip

# How to run

1. Make sure you are in the root directory where `run.py` exist
2. The script takes one positional argument:

- --url - a valid URL. Must include http/https

3.  Run the following command to test the script

        python run.py --url https://www.google.com

# Developer Notes / Todos

- Tested with HTML. Should work with XML as well, but not tested.
- If to be used on scale, - proxy support and rotating browser agent should be implemented. Can be done on BaseConnection class.
- Retry strategy implemented. Will retry request if previous one response status code is in the pre-defined list.
- Todo: write more tests.
- Code base is formatted using Black code formatter - https://github.com/psf/black - Highly recommended.
